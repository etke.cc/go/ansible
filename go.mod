module gitlab.com/etke.cc/go/ansible

go 1.21

require (
	golang.org/x/exp v0.0.0-20240416160154-fe59bbe5cc7f
	gopkg.in/yaml.v3 v3.0.1
)
